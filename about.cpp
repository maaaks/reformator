#include "about.h"
#include "ui_about.h"

About::About(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::About)
{
	ui->setupUi(this);
	ui->tabWidget->setCurrentIndex(0);
}

About::~About()
{
	delete ui;
}

void About::on_buttonBox_accepted()
{
	delete this;
}
