#ifndef CLIPBOARDDIALOG_H
#define CLIPBOARDDIALOG_H

#include <QtWidgets/QAbstractButton>
#include <QtWidgets/QDialog>

namespace Ui {
class ClipboardDialog;
}

class ClipboardDialog : public QDialog
{
	Q_OBJECT
	
public:
	explicit ClipboardDialog(QWidget *parent = 0);
	~ClipboardDialog();
	
	QString bookName();
	QString bookText();
	
private slots:
	void on_bookName_textEdited(const QString &arg1);
	void on_bookText_textChanged();
	
private:
	Ui::ClipboardDialog *ui;
};

#endif // CLIPBOARDDIALOG_H
