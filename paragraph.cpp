#include "paragraph.h"

Paragraph::Paragraph(QString text)
{
	_text = text;
	rescanProperties();
}


/** Update cache values for all properties of the paragraph.
 */
void Paragraph::rescanProperties()
{
	// If the last symbol of the paragraph is a hyphen, remember this fact
	_hasHyphen = _text.trimmed().endsWith("-");
	
	// Remember how many spaces does the paragraph has at the beginning
	_indent = 0;
	foreach (QChar ch, _text) {
		if (ch.category() == QChar::Separator_Space)
			_indent++;
		else
			break;
	}
	
	// Remember does the paragraph seem to be justified or not: search for double space
	_hasJustify = _text.trimmed().contains("  ");
}


/** Returns the string containing inide the paragraph.
 */
QString Paragraph::text()
{
	return _text;
}
void Paragraph::setText(QString text)
{
	_text = text;
	rescanProperties();
}


/** Append given text to existing one.
 */
void Paragraph::append(QString text)
{
	_text += text;
	rescanProperties();
}
void Paragraph::append(Paragraph paragraph)
{
	QString text = paragraph.text();
	_text += text;
	rescanProperties();
}


/** Returns text's length.
 */
int Paragraph::length()
{
	return _text.length();
}


/** Returns true if the last symbol of the paragraph is a hyphen.
 */
bool Paragraph::hasHyphen()
{
	return _hasHyphen;
}


/** Returns number of spaces at the beginning of the paragraph.
 */
int Paragraph::indent()
{
	return _indent;
}


/** Returns true if the paragraph seems to be justified.
  
  It means that it returns true only if paragraph has two spaces somewhere except start and end.
 */
bool Paragraph::isJustified()
{
	return _hasJustify;
}