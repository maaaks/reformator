#ifndef BOOK_H
#define BOOK_H

#include <QtCore/QList>
#include <QtCore/QUrl>
#include "paragraph.h"

/** A book: composed of Paragraphs and manages merging them.
  
  Initially, a Book should be coinstructed with a string containing all the text loaded,
  and it will analyze the text and automatically detect optimal settings for it.
  You can set custom settings if needed and then call reformat() and export()
  to get the readable version of the text.
  
  In addition, the Book object contains URL of the source file it was loaded from.
  If the book is created without original file, the URL will be null.
  In that case, book's name will be used when choosing output file (see MainWindow::on_go_clicked()).
 */
class Book
{
	QList<Paragraph> _paragraphs;
	QString _name;
	QUrl _sourceFile;
	
	int _maximumLength;
	int _paragraphIndent;
	bool _removeJustify;
	bool _removeWordWrap;
	
public:
	Book(QString text, QUrl sourceFile=QUrl(), QString name="");
	
	void autoDetectSettings();
	QString text();
	void reformat();
	
	// Static methods
	static Book metaBook(QList<Book*> books);
	
	// Getters for meta information
	QUrl sourceFile();
	QString name();
	
	// Getters for settings
	int optionParagraphIndent();
	bool optionRemoveJustify();
	bool optionRemoveWordWrap();
	
	// Setters for settings
	void setOptionParagraphIndent(int paragraphIndent);
	void setOptionRemoveJustify(bool removeJustify);
	void setOptionRemoveWordWrap(bool removeWordWrap);
};

#endif // BOOK_H
