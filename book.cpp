#include "book.h"

#include <QtCore/QFileInfo>
#include <QtCore/QMap>
#include <QtCore/QStringList>

/** Constructor. Splits the text into lines and call setAutoSettings() to detect parameters.
  
  The splitted text is then stored in the Book which is a QList<Paragraph>.
 */
Book::Book(QString text, QUrl sourceFile, QString name)
{
	// Default values for options
	_maximumLength = 80;
	_paragraphIndent = 4;
	_removeJustify = false;
	_removeWordWrap = true;
	
	// Information useful for choosing output file
	_sourceFile = sourceFile;
	_name = (name.isEmpty() && !sourceFile.isEmpty())
		? QFileInfo(sourceFile.path()).fileName()
		: name;
	
	// Loading given text
	foreach (QString rawParagraph, text.split("\n")) {
		_paragraphs << Paragraph(rawParagraph);
	}
}

/** Automatically sets optimal settings for the book.
  
  The settings are chosen based on statistics of different types of lines in it.
 */
void Book::autoDetectSettings()
{
	int paragraphsCount = _paragraphs.count();
	int paragraphIndent, paragraphLength;
	
	// 1. Collect information about how many times different indent are used in the text...
	QMap<int,int> indentStat;
	for (int i=0; i<paragraphsCount; i++) {
		paragraphIndent = _paragraphs[i].indent();
		if (!indentStat.contains(paragraphIndent)) {
			indentStat[paragraphIndent] = 1;
		} else {
			indentStat[paragraphIndent]++;
		}
	}
	
	// ...and select the most frequent indent (except zero indent)
	_paragraphIndent = 0;
	foreach (int indent, indentStat.keys()) {
		if (indent != 0 &&
			(_paragraphIndent == 0 || indentStat[indent] > indentStat[_paragraphIndent])
		) {
			_paragraphIndent = indent;
		}
	}
	
	
	// 2. Select maximum length of a line
	_maximumLength = 0;
	for (int i=0; i<paragraphsCount; i++) {
		paragraphLength = _paragraphs[i].length();
		if (paragraphLength > _maximumLength) {
			_maximumLength = paragraphLength;
		}
	}
	
	
	// 3. Now, when we know the maximum length, look for paragraphs which seem to be wrapped...
	int wordWrapCount = 0;
	for (int i=0; i<paragraphsCount-1; i++) {
		if (_paragraphs[i].hasHyphen() && !_paragraphs[i+1].text()[0].isSpace()) {
			wordWrapCount++;
		}
	}
	
	// ...and set the option if the wordWrapCount is big enough (it doesn't have sense, really)
	_removeWordWrap = ((float)wordWrapCount / _paragraphs.length() >= 0.2);
	
	
	// 4. Set «Remove Justify» option if there is at least one justified line
	_removeJustify = false;
	for (int i=0; i<paragraphsCount; i++) {
		if (_paragraphs[i].length() == _maximumLength && _paragraphs[i].isJustified()) {
			_removeJustify = true;
			break;
		}
	}
}


/** Returns all the text of the book joined together.
  */
QString Book::text()
{
	QString text;
	foreach (Paragraph paragraph, _paragraphs) {
		text += paragraph.text() += "\n";
	}
	return text;
}


/** Join all paragraphs from given books and make up a big «meta book».
  
  Useful for getting total statistics before starting a batch converting.
 */
Book Book::metaBook(QList<Book*> books)
{
	QString metaText;
	
	foreach (Book *book, books) {
		metaText += book->text();
	}
	
	return Book(metaText);
}


/** This settings stores the number of spaces in the first line of a paragraph.
 */
int Book::optionParagraphIndent()
{
	return _paragraphIndent;
}

void Book::setOptionParagraphIndent(int paragraphIndent)
{
	_paragraphIndent = paragraphIndent;
}


/** Returns true if the book seems to contain enough word wraps.
 */
bool Book::optionRemoveWordWrap()
{
	return _removeWordWrap;
}

void Book::setOptionRemoveWordWrap(bool removeWordWrap)
{
	_removeWordWrap = removeWordWrap;
}


/** Returns true if the book seems to be justified.
 */
bool Book::optionRemoveJustify()
{
	return _removeJustify;
}

void Book::setOptionRemoveJustify(bool removeJustify)
{
	_removeJustify = removeJustify;
}


/** Performs book «reformatting» using previously set options.
  
  The most inportant mthod in the whole program. :-)
 */
void Book::reformat()
{
	QString text;
	
	for (int i=0; i<_paragraphs.count(); i++)
	{
		// If we're checking indents, only a correct-indented paragraph will remain paragraph
		if (i>0 && _paragraphIndent>0 && _paragraphs[i].indent()!=_paragraphIndent)
		{
			// Append current paragraph to the previous one
			// If previous paragraph seems to end with hyphen, append current paragraph to it
			if (_removeWordWrap && _paragraphs[i-1].hasHyphen() && !_paragraphs[i].text()[0].isSpace()) {
				text = _paragraphs[i-1].text().trimmed();
				text = text.left(text.length()-1);
				_paragraphs[i-1].setText(text + _paragraphs[i].text());
			// Else, simply append current text to previous (adding a space between them)
			} else {
				_paragraphs[i-1].append(" ");
				_paragraphs[i-1].append(_paragraphs[i]);
			}
			
			// Remove current paragraph (beacuse it's already added to previous)
			_paragraphs.removeAt(i);
			i--;
			
		// If the paragraph has appropriate indent, remove it
		// (after reformatting, a reader doesn't need such indents)
		} else {
			_paragraphs[i].setText(_paragraphs[i].text().trimmed());
		}
	}
}


/** Returns URL of file the book was loaded from.
 */
QUrl Book::sourceFile()
{
	return _sourceFile;
}


/** Returns the name of the book.
  
  If the book was loaded from file, this is usually name of that file.
 */
QString Book::name()
{
	return _name;
}
