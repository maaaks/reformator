#ifndef PARAGRAPH_H
#define PARAGRAPH_H

#include <QtCore/QString>

/** A line or paragraph of text.
  
  After a Book is just loaded, every Paragraph object usually contains one line.
  While processing, Paragraphs which are recognized as non-paragraphs, are being added to their previous siblings.
  Finally, if the configuration was correct, every Paragraph should contain one real paragraph of text.
 */
class Paragraph
{
	QString _text;
	bool _hasHyphen;
	bool _hasJustify;
	int _indent;
	
protected:
	void rescanProperties();
	
public:
	Paragraph(QString text="");
	
	// Internal string relatedmethods
	QString text();
	void setText(QString text);
	void append(QString text);
	void append(Paragraph paragraph);
	int length();
	
	// Getters for paragraph-specific information
	bool hasHyphen();
	int indent();
	bool isJustified();
};

#endif // PARAGRAPH_H
