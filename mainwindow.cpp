#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QtCore/QFile>
#include <QtCore/QMimeData>
#include <QtCore/QTextStream>
#include <QtGui/QDragEnterEvent>
#include <QtWidgets/QFileDialog>
#include "about.h"
#include "clipboarddialog.h"

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);
	setAcceptDrops(true);
	
	// Set icons from theme or (on Windows) from resources
	ui->addFile->setIcon(QIcon::fromTheme("folder-open", QIcon("://icons/noun/1251.png"))); // Sergio Calcara, from The Noun Project
	ui->addText->setIcon(QIcon::fromTheme("edit-paste", QIcon("://icons/noun/1714.png"))); // Seth Taylor
	ui->removeBook->setIcon(QIcon::fromTheme("list-remove", QIcon("://icons/noun/951.png"))); // Andre, from The Noun Project
	ui->autoDetectSettings->setIcon(QIcon::fromTheme("system-run", QIcon("://icons/noun/1064.png"))); // Jeremy Minnick
	ui->outputFolder->setIcon(QIcon::fromTheme("folder-open", QIcon("://icons/noun/1251.png"))); // Sergio Calcara, from The Noun Project
	ui->go->setIcon(QIcon::fromTheme("media-playback-start", QIcon("://icons/noun/2873.png"))); // P.J. Onori, from The Noun Project
	ui->about->setIcon(QIcon::fromTheme("help-about", QIcon("://icons/noun/908.png"))); // Sven Hofmann, from The Noun Project
	ui->exit->setIcon(QIcon::fromTheme("application-exit", QIcon("://icons/noun/2466.png"))); // Andrew S
	
	// Set default directory for output
	ui->outputFolder->setText(QDir::homePath());
}

MainWindow::~MainWindow()
{
	delete ui;
}


/** Add items into the books list.
  
  This ignores the files which have been added before.
 */
void MainWindow::dropEvent(QDropEvent *event)
{
	foreach (QUrl url, event->mimeData()->urls()) {
		addFromFile(url);
	}
}


/** Add a URL-based item into books list (if the URL isn't added yet).
 */
void MainWindow::addFromFile(QUrl url)
{
	QListWidgetItem* item;
	QVariant data;
	int itemsCount = ui->booksList->count();
	
	// Iterate through existing items looking for the one corresponding to this file
	for (int i=0; i<itemsCount; i++) {
		item = ui->booksList->item(i);
		data = item->data(Qt::UserRole);
		
		// The URL stored as Qt::UserRole (if the item is URL-based, not clipboard-based)
		if (data.type()==QVariant::Url && data.toUrl()==url)
		{
			// Ok, the item is already here. Do nothing.
			return;
		}
	}
	
	// If the file haven't been added before, add it
	QIcon icon = QIcon::fromTheme("text-x-generic", QIcon("://icons/noun/2222.png")); // Carlo, from The Noun Project
	QString name = QFileInfo(url.path()).baseName();
	addFromGivenData(icon, name, url);
}


/** Add an item into books list using not an URL, but the given data.
  
  @see addFromFile()
 */
void MainWindow::addFromGivenData(QIcon icon, QString name, QVariant data)
{
	QListWidgetItem *item = new QListWidgetItem(icon, name);
	item->setData(Qt::UserRole, data);
	ui->booksList->addItem(item);
}


/** Creates a list of all books.
  
  The method is called only before «real» operations such as «Auto detect settings» and «Reformat!».
  For all other interface actions, QListWidget without full book data is enough.
 */
QList<Book*> MainWindow::books()
{
	QList<Book*> books;
	QListWidgetItem *item;
	QVariant data;
	QFile *file;
	QString text;
	
	for (int i=0; i<ui->booksList->count(); i++) {
		item = ui->booksList->item(i);
		data = item->data(Qt::UserRole);
		
		if (data.type() == QVariant::Url) {
			// Open file with given URL
			file = new QFile(data.toUrl().toLocalFile());
			file->open(QIODevice::ReadOnly|QIODevice::Text);
			
			// Read the text into the new book
			text = QString::fromUtf8(file->readAll());
			books << new Book(text, data.toUrl());
			
			// Close file
			file->close();
			
		} else {
			// The full text is already loaded (from clipboard or manually)
			books << new Book(data.toString());
		}
	}
	
	return books;
}

void MainWindow::on_autoDetectSettings_clicked()
{
	// Detect settings
	Book metaBook = Book::metaBook(books());
	metaBook.autoDetectSettings();
	
	// Show the detected settings
	ui->spacesBeforeParagraph->setValue(metaBook.optionParagraphIndent());
	ui->removeJustify->setChecked(metaBook.optionRemoveJustify());
	ui->removeWordWrap->setChecked(metaBook.optionRemoveWordWrap());
}



/** Start reformatting all the books!
 */
void MainWindow::on_go_clicked()
{
	QString fileName;
	QFile *file;
	
	// Collect output configuration
	// It won't change during the process, so let's check it only once.
	// I think it will be the only optimization in this program. :-)
	bool overwriteOriginal = ui->overwriteSourceFiles->isChecked();
	QString outputFolder = ui->outputFolder->text();
	
	// Get all the books from the list and reformat each of them.
	foreach (Book *book, books())
	{
		// Reformat book
		book->setOptionParagraphIndent(ui->spacesBeforeParagraph->value());
		book->setOptionRemoveJustify(ui->removeJustify->isChecked());
		book->setOptionRemoveWordWrap(ui->removeWordWrap->isChecked());
		book->reformat();
		
		// Decide where to output the book
		if (overwriteOriginal && !book->sourceFile().isEmpty()) {
				fileName = book->sourceFile().path();
		} else {
			fileName = outputFolder + QDir::separator() + book->name();
		}
		
		// Save book to the file
		file = new QFile(fileName);
		file->open(QIODevice::WriteOnly|QIODevice::Text);
		QTextStream(file) << book->text();
		file->close();
	}
}

void MainWindow::on_outputTo_toggled(bool checked)
{
    ui->outputFolder->setEnabled(checked);
}


/** Accept every file dragged to the window.
  
  @see dropEvent()
 */
void MainWindow::dragEnterEvent(QDragEnterEvent *event)
{
	event->acceptProposedAction();
}

void MainWindow::on_outputFolder_clicked()
{
	QString directory = QFileDialog::getExistingDirectory(this, "", ui->outputFolder->text());
	ui->outputFolder->setText(directory);
}

void MainWindow::on_addFile_clicked()
{
	QStringList paths = QFileDialog::getOpenFileNames(this, "", ui->outputFolder->text());
	foreach (QString path, paths) {
		addFromFile(QUrl::fromLocalFile(path));
	}
}

void MainWindow::on_exit_clicked()
{
    QApplication::quit();
}

void MainWindow::on_removeBook_clicked()
{
	QListWidgetItem *item = ui->booksList->currentItem();
    ui->booksList->removeItemWidget(item);
	delete item;
	
	if (ui->booksList->count() == 0) {
		ui->removeBook->setEnabled(false);
	}
}

void MainWindow::on_booksList_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous)
{
    ui->removeBook->setEnabled(true);
}

void MainWindow::on_addText_clicked()
{
    ClipboardDialog *clipboardDialog = new ClipboardDialog;
	clipboardDialog->exec();
	if (clipboardDialog->result() == QDialog::Accepted) {
		QIcon icon = QIcon::fromTheme("edit-paste", QIcon("://icons/noun/1714.png"));
		QString name = clipboardDialog->bookName() + ".txt";
		QString data = clipboardDialog->bookText();
		addFromGivenData(icon, name, data);
	}
}

void MainWindow::on_about_clicked()
{
	About *about = new About;
	about->show();
}
