#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtCore/QUrl>
#include <QtWidgets/QListWidgetItem>
#include <QtWidgets/QMainWindow>
#include "book.h"

namespace Ui {
	class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT
	
	Ui::MainWindow *ui;
	
public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();
	
protected:
	void dragEnterEvent(QDragEnterEvent *event);
	void dropEvent(QDropEvent *event);
	QList<Book*> books();
	void addFromFile(QUrl url);
	void addFromGivenData(QIcon icon, QString name, QVariant data);
	
private slots:
	void on_about_clicked();
	void on_addFile_clicked();
	void on_addText_clicked();
	void on_autoDetectSettings_clicked();
	void on_booksList_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous);
	void on_exit_clicked();
	void on_go_clicked();
	void on_outputFolder_clicked();
	void on_outputTo_toggled(bool checked);
	void on_removeBook_clicked();
};

#endif // MAINWINDOW_H
