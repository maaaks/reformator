#include "clipboarddialog.h"
#include "ui_clipboarddialog.h"

#include <QtWidgets/QPushButton>

ClipboardDialog::ClipboardDialog(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::ClipboardDialog)
{
	ui->setupUi(this);
	ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
}

ClipboardDialog::~ClipboardDialog()
{
	delete ui;
}
void ClipboardDialog::on_bookName_textEdited(const QString &arg1)
{
	on_bookText_textChanged();
}

void ClipboardDialog::on_bookText_textChanged()
{
	if (bookName().length()>0 && bookText().length()>0) {
		ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(true);
	} else {
		ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
	}
}

QString ClipboardDialog::bookName()
{
	return ui->bookName->text();
}

QString ClipboardDialog::bookText()
{
	return ui->bookText->document()->toPlainText();
}
